# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views

urlpatterns = [
    # URL pattern for the HomepageView
    url(
        regex=r'^$',
        view=views.HomepageView.as_view(),
        name='homepage'
    ),
]
