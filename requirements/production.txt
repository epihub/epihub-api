# Pro-tip: Try not to put anything here. There should be no dependency in
#	production that isn't in development.
-r base.txt

# Database Handler (PostgreSQL)
# ------------------------------------------------
psycopg2==2.6.1

# WSGI Handler
# ------------------------------------------------

gevent==1.1.1
greenlet==0.4.10

gunicorn==19.6.0
