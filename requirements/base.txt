defusedxml==0.4.1
Django==1.9.7
django-allauth==0.25.2
django-autoslug==1.9.3
django-braces==1.9.0
django-environ==0.4.0
django-nested-inline==0.3.6
oauthlib==1.1.2
Pillow==3.2.0
python3-openid==3.0.10
requests==2.10.0
requests-oauthlib==0.6.1
six==1.10.0
sqlparse==0.1.19

